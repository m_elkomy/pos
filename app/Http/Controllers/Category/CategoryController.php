<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStore;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_categories'])->only('index');
        $this->middleware(['permission:create_categories'])->only('create');
        $this->middleware(['permission:update_categories'])->only('edit');
        $this->middleware(['permission:delete_categories'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('users.categories.index',['title'=>trans('users.Category List'),'categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.categories.create',['title'=>trans('users.Add Category')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStore $request)
    {
        $request->validated();
        $category = new Category();
        $category->name = ['en'=>$request->name_en,'ar'=>$request->name_ar];
        $category->save();
        session()->flash('success',trans('users.Data Saved Successfully'));
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('users.categories.edit',['title'=>trans('users.Category Edit'),'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryStore $request, $id)
    {
        $request->validated();
        $category = Category::find($id);
        $category->update([
            $category->name = ['en'=>$request->name_en,'ar'=>$request->name_ar],
        ]);
        session()->flash('success',trans('users.Data Updated Successfully'));
        return redirect(aurl('categories'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        session()->flash('success',trans('users.Data Deleted Successfully'));
        return back();
    }

    public function multi_delete(Request $request){
        $items = $request->items;
        if(!empty($items)){
            if(is_array($items)){
                Category::destroy($items);
            }else{
                Category::delete($items);
            }
            session()->flash('success',trans('users.Data Deleted Successfully'));
            return back();
        }else{
            session()->flash('error',trans('users.You Must Choose Some Record'));
            return back();
        }
    }

    public function multi_add(){
        return view('users.categories.mutli_add',['title'=>trans('users.Multi Add Category')]);
    }
    public function multi_store(Request $request){
        $data = $request->validate([
            'category_list.*.name_ar'=>'required|unique:categories,name->ar',
            'category_list.*.name_en'=>'required|unique:categories,name->en',
        ]);

        $category_list = $request->category_list;
        foreach ($category_list as $list){
            $category = new Category();
            $category->name = ['en'=>$list['name_en'],'ar'=>$list['name_ar']];
            $category->save();
        }
        session()->flash('success',trans('users.Data Saved Successfully'));
        return redirect(aurl('categories'));
    }
}
