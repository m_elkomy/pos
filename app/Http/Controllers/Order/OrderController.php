<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_orders'])->only('index');
        $this->middleware(['permission:create_orders'])->only('create');
        $this->middleware(['permission:update_orders'])->only('edit');
        $this->middleware(['permission:delete_orders'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('users.orders.index',['title'=>trans('users.Orders List'),'orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::with('products')->get();
        return view('users.orders.create',['title'=>trans('users.Add Order'),'categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quantities'=>'required|array',
            'products'=>'required|array'
        ]);
        $total_price = 0;
        $order = Order::create([]);
        foreach ($request->products as $index=>$product){
            $pro = Product::findOrFail($product);
            $total_price+=$pro->sale_price * $request->quantities[$index];
            $order->products()->attach($product,['quantity'=>$request->quantities[$index]]);
            $pro->update([
                'stock'=>$pro->stock - $request->quantities[$index]
            ]);
        }

        $order->update([
            'total_price'=>$total_price
        ]);
        session()->flash('success',trans('users.Data Saved Successfully'));
        return redirect(aurl('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $product = $order->products;
        return view('users.orders.show',['title'=>trans('users.Show Order'),'products'=>$product,'order'=>$order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $categories = Category::all();
        return view('users.orders.edit',['title'=>trans('users.Order Edit'),'order'=>$order,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Order $order)
    {
        $request->validate([
            'quantities'=>'required|array',
            'products'=>'required|array'
        ]);

        foreach ($order->products as $product){
            $product->update([
                'stock'=>$product->stock + $product->pivot->quantity
            ]);
        }
        $order->delete();

        $total_price = 0;
        $order = Order::create([]);
        foreach ($request->products as $index=>$product){
            $pro = Product::findOrFail($product);
            $total_price+=$pro->sale_price * $request->quantities[$index];
            $order->products()->attach($product,['quantity'=>$request->quantities[$index]]);
            $pro->update([
                'stock'=>$pro->stock - $request->quantities[$index]
            ]);
        }

        $order->update([
            'total_price'=>$total_price
        ]);

        session()->flash('success',trans('users.Data Updated Successfully'));
        return redirect(aurl('orders'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        foreach ($order->products as $product){
            $product->update([
                'stock'=>$product->stock + $product->pivot->quantity
            ]);
        }
        $order->delete();
        session()->flash('success',trans('users.Data Deleted Successfully'));
        return redirect(aurl('orders'));
    }

}
