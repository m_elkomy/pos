<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_products'])->only('index');
        $this->middleware(['permission:create_products'])->only('create');
        $this->middleware(['permission:update_products'])->only('edit');
        $this->middleware(['permission:delete_products'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('users.products.index',['title'=>trans('users.Products List'),'products'=>$products,'categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('users.products.create',['title'=>trans('users.Add Product'),'categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->validated();
        $prodcut = new Product();
        $prodcut->name = ['en'=>$request->name_en,'ar'=>$request->name_ar];
        $prodcut->category_id = $request->category_id;
        $prodcut->stock = $request->stock;
        $prodcut->sale_price = $request->sale_price;
        $prodcut->purchase_price = $request->purchase_price;
        if($request->image){
            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/products/'.$request->image->hashName()));
            $prodcut['image'] = $request->image->hashName();
        }
        $prodcut->save();
        session()->flash('success',trans('users.Data Saved Successfully'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        return view('users.products.edit',['title'=>trans('users.Product Edit'),'product'=>$product,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $request->validated();
        $product = Product::find($id);
        $product->name = ['en'=>$request->name_en,'ar'=>$request->name_ar];
        $product->category_id = $request->category_id;
        $product->sale_price = $request->sale_price;
        $product->purchase_price = $request->purchase_price;
        $product->stock = $request->stock;
        if($request->image){
            if($product->image != 'default.png'){
                Storage::disk('public_uploads')->delete('products/'.$product->image);
            }

            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/products/'.$request->image->hashName()));
            $product['image'] = $request->image->hashName();
        }
        $product->save();
        session()->flash('success',trans('users.Data Updated Successfully'));
        return redirect(aurl('products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product->image!='default.png'){
            Storage::disk('public_uploads')->delete('products/'.$product->image);
        }
        $product->delete();
        session()->flash('success',trans('users.Data Deleted Successfully'));
        return back();
    }

    public function multi_delete(Request $request){
        $items = $request->items;
        if(!empty($items)){
            if(is_array($items)){
                foreach ($items as $item){
                    $product = Product::find($item);
                    if($product->image !='default.png'){
                        Storage::disk('public_uploads')->delete('products/'.$product->image);
                    }
                    Product::where('id',$product->id)->delete();
                }
                session()->flash('success',trans('admin.Data Deleted Successfully'));
                return redirect(aurl('products'));

            }else{
                Product::delete($items);
                session()->flash('success',trans('admin.Data Deleted Successfully'));
                return redirect(aurl('products'));
            }
        }else{
            session()->flash('error',trans('users.You Must Choose Some Record'));
            return back();
        }
    }

    public function getcategory(Request $request){
        $categories = Category::all();
        $category_id = $request->category_id;
        if($category_id!=0){
            $products = Product::where('category_id',$request->category_id)->get();
            return view('users.products.index',['title'=>trans('users.Products List'),'products'=>$products,'categories'=>$categories]);
        }else{
            $products = Product::all();
            return view('users.products.index',['title'=>trans('users.Products List'),'products'=>$products,'categories'=>$categories]);
        }

    }

    public function multi_add(){
        $categories = Category::all();
        return view('users.products.multi_add',['title'=>trans('users.Multi Add Product'),'categories'=>$categories]);
    }

    public function multi_store(Request $request)
    {
        $request->validate([
            'products_list.*.name_en' => 'required',
            'products_list.*.name_ar' => 'required',
            'products_list.*.category_id' => 'required|exists:categories,id',
            'products_list.*.purchase_price' => 'required',
            'products_list.*.sale_price' => 'required',
            'products_list.*.stock' => 'required',
        ]);
        $list_products = $request->products_list;
        foreach ($list_products as $list) {
            $product = new Product();
            $product->name = ['en' => $list['name_en'], 'ar' => $list['name_ar']];
            $product->category_id = $list['category_id'];
            $product->purchase_price = $list['purchase_price'];
            $product->sale_price = $list['sale_price'];
            $product->stock = $list['stock'];
            $product->save();
        }
        session()->flash('success', trans('users.Data Saved Successfully'));
        return redirect(aurl('products'));
    }
}
