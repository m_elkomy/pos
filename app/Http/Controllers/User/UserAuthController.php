<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\UserResetPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class UserAuthController extends Controller
{
    //function to return login view
    public function login(){
        return view('users.login',['title'=>trans('users.Login')]);
    }

    //function to handle users login
    public function do_login(Request $request){
        $remember_me = $request->remember_me == 1 ? true : false;
        if(admin()->attempt(['email'=>$request->email,'password'=>$request->password],$remember_me)){
            session()->flash('success',trans('users.You Are Logged In Successfully'));
            return redirect(aurl());
        }else{
            session()->flash('error',trans('users.Sorry There Were An Error'));
            return back()->withInput();
        }
    }

    //function to handle user logout
    public function logout(){
        admin()->logout();
        session()->flash('success',trans('users.You Are Logged Out Successfully'));
        return redirect('login');
    }

    //function to return forget password view
    public function forget_password(){
        return view('users.forget_password',['title'=>trans('users.Forgot Password?')]);
    }

    //function to create token for user
    public function forget_password_post(Request $request){
        $user = User::where('email',$request->email)->first();
        if(!empty($user)){
            $token = app('auth.password.broker')->createToken($user);
            $data = DB::table('password_resets')->insert([
                'email'=>$user->email,
                'token'=>$token,
                'created_at'=>Carbon::now()
            ]);
            Mail::to($user->email)->send(new UserResetPassword(['data'=>$user,'token'=>$token]));
            session()->flash('success',trans('users.Reset Link Sent Successfully'));
            return back()->withInput();
        }else{
            session()->flash('error',trans('users.Sorry There Were An Error'));
            return back()->withInput();
        }
    }

    //function to return reset password view
    public function reset_password($token){
        $check_token = DB::table('password_resets')->where('token',$token)->where('created_at','>',Carbon::now()->subHours(2))->first();
        if(!empty($check_token)){
            return view('users.reset_password',['title'=>trans('users.Reset Password'),'data'=>$check_token]);
        }else{
            session()->flash('error',trans('users.Sorry There Were An Error'));
            return back()->withInput();
        }
    }

    //function to reset user password
    public function reset_password_final(Request $request,$token){
        $check_token = DB::table('password_resets')->where('token',$token)->where('created_at','>',Carbon::now()->subHours(2))->first();
        $request->validate([
            'email'=>['required',Rule::in($check_token->email)],
            'password'=>'required|confirmed|min:9',
            'password_confirmation'=>'required'
        ]);
        if(!empty($check_token)){
            $user = User::where('email',$check_token->email)->update([
                'email'=>$check_token->email,
                'password'=>bcrypt($request->password)
            ]);
            DB::table('password_resets')->where('email',$request->email)->delete();
            $serialNumber = SerialNumber();
            admin()->attempt(['email'=>$check_token->email,'password'=>$request->password,'serialNumber'=>$serialNumber],true);
            session()->flash('success',trans('users.Password Reset Successfully'));
            return redirect(aurl());
        }else{
            session()->flash('error',trans('users.Sorry There Were An Error'));
            return back()->withInput();
        }
    }
}
