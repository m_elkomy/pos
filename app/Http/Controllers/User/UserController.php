<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStore;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_users'])->only('index');
        $this->middleware(['permission:create_users'])->only('create');
        $this->middleware(['permission:update_users'])->only('edit');
        $this->middleware(['permission:delete_users'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereRoleIs('admin')->get();
        return view('users.users.index',['title'=>trans('users.Users List'),'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.users.create',['title'=>trans('users.Add User')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStore $request)
    {
        $serialNumber = serialNumber();
        $data = $request->validated();
        if($request->image){
            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/'.$request->image->hashName()));
            $data['image'] = $request->image->hashName();
        }
        if($request->password){
            $data['password'] = bcrypt($request->password);
        }
        $user = User::create($data);
        $user->attachRole('admin');
        $user->syncPermissions($request->permissions);
        session()->flash('success',trans('users.Data Saved Successfully'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.users.edit',['title'=>trans('users.User Edit'),'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name'=>'required|min:5',
            'email'=>['required',Rule::unique('users','email')->ignore($id,'id')],
            'password'=>'sometimes|nullable|min:9',
            'permissions'=>'required|array|min:1',
            'image'=>'mimes:jpeg,gif,jpg,png',
        ]);
        $user = User::find($id);
        if($request->has('password')){
            $data['password'] = bcrypt($request->password);
        }
        if($request->image){
            if($request->image !='default.png'){
                Storage::disk('public_uploads')->delete('users/'.$user->image);
            }

            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/'.$request->image->hashName()));
            $data['image'] = $request->image->hashName();
        }
        $user->update($data);
        $user->syncPermissions($request->permissions);
        session()->flash('success',trans('users.Data Updated Successfully'));
        return redirect(aurl('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->image!='default.png'){
            Storage::disk('public_uploads')->delete('users/'.$user->image);
        }
        $user->delete();
        session()->flash('success',trans('users.Data Deleted Successfully'));
        return back();
    }
}
