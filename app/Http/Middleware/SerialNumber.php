<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class SerialNumber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $serialNumber = shell_exec('wmic bios get serialnumber');
        $remove =  preg_replace('~[\r\n]+~', '', $serialNumber);
        $trim = rtrim($remove);
        $replace = str_replace('SerialNumber  ','',$trim);
        $user = User::where('serialNumber',$replace)->first();
        if(!empty($user)){
            return $next($request);
        }else{
            return redirect(route('notFound'));
        }
    }
}
