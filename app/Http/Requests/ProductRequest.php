<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'=>'required',
            'name_en'=>'required',
            'category_id'=>['required',Rule::exists('categories','id')],
            'stock'=>'required',
            'purchase_price'=>'required',
            'sale_price'=>'required',
            'image'=>'sometimes|nullable|mimes:jpg,jpeg,gif,png'
        ];
    }
}
