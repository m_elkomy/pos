<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:5',
            'email'=>'required|unique:users,email,'.$this->id,
            'password'=>'required|min:9|confirmed',
            'password_confirmation'=>'required',
            'permissions'=>'required|array|min:1',
            'image'=>'image|mimes:jpg,jpeg,gif,png'
        ];
    }
}
