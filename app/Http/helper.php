<?php
//function to return admin url
if(!function_exists('aurl')){
    function aurl($url =null){
        return url('dashboard/'.$url);
    }
}

//function to return web guard
if(!function_exists('admin')){
    function admin(){
        return auth()->guard('web');
    }
}

//function to get serial number
if(!function_exists('serialNumber')){
    function serialNumber(){
        $serialNumber = shell_exec('wmic bios get serialnumber');
        $remove =  preg_replace('~[\r\n]+~', '', $serialNumber);
        $trim = rtrim($remove);
        $replace = str_replace('SerialNumber  ','',$trim);
        return $replace;
    }
}

//function to make active menu
if(!function_exists('active_menu')){
    function active_menu($link){
        if(preg_match('/'.$link.'/i',Request::segment(3))){
            return ['aria-expanded="true"','','show'];
        }else{
            return ['aria-expanded="true"','collapsed',''];
        }
    }
}

//function to return active link
if(!function_exists('active_link')){
    function active_link($link){
        if(request()->is('en/dashboard/'.$link)){
            return ['active'];
        }else{
            return [''];
        }
    }
}
