<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use HasFactory,HasTranslations;
    protected $table = 'products';
    protected $fillable = ['name','category_id','stock','sale_price','purchase_price','image'];
    protected $translatable = ['name'];
    protected $appends = ['image_path','profit_percent'];
    public function getImagePathAttribute(){
        return asset('uploads/products/'.$this->image);
    }

    public function getProfitPercentAttribute(){
        $profit = $this->sale_price - $this->purchase_price;
        $profit_percent = $profit * 100 / $this->purchase_price;
        return $profit_percent;
    }

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function orders(){
        return $this->belongsTo(Order::class,'product_order');
    }
}
