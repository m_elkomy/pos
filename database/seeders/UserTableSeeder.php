<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serialNumber = shell_exec('wmic bios get serialnumber');
        $remove =  preg_replace('~[\r\n]+~', '', $serialNumber);
        $trim = rtrim($remove);
        $replace = str_replace('SerialNumber  ','',$trim);
        $user = User::create([
            'name'=>'elkomy',
            'email'=>'mohamed.s.elkomy@gmail.com',
            'password'=>bcrypt('123456'),
            'serialNumber'=>$replace
        ]);

        $user->attachRole('super_admin');
    }
}
