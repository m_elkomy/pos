@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">


        <div class="col-lg-12 col-12  layout-spacing">
            @include('users.layout.message')

            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content">

                    <form method="post" action="{{route('categories.store')}}">
                        {{csrf_field()}}
                        <div class="form-group mb-4">
                            <label for="name_ar">{{trans('users.Arabic Name')}}</label>
                            <input type="text" name="name_ar" class="form-control" id="name_ar" placeholder="{{trans('users.Arabic Name')}}" value="{{old('name_ar')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="name_en">{{trans('users.English Name')}}</label>
                            <input type="text" name="name_en" class="form-control" id="name_en" placeholder="{{trans('users.English Name')}}" value="{{old('name_en')}}">
                        </div>
                        <button type="submit" class="btn btn-info">{{trans('users.Save')}}</button>
                        <a href="{{aurl('categories')}}" class="btn btn-danger">{{trans('users.Cancel')}}</a>
                    </form>

                </div>
            </div>
        </div>

    </div>
@endsection
