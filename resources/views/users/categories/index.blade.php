@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/plugins/animate/animate.css" rel="stylesheet" type="text/css" />
    <script src="{{url('/')}}/admin/plugins/sweetalerts/promise-polyfill.js"></script>
    <link href="{{url('/')}}/admin/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')

    <div class="row layout-top-spacing">

        @include('users.layout.message')
        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <form action="{{route('category_multi_delete')}}" id="mutlidel">
                {{csrf_field()}}
                @if(auth()->user()->hasPermission('delete_categories'))
                <button type="submit" class="btn btn-danger mb-3 multidel">{{trans('users.Delete Selected')}}</button>
                @else
                <button type="button" class="btn btn-danger mb-3 disabled">{{trans('users.Delete Selected')}}</button>

                @endif

                <div class="widget-content widget-content-area br-6">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                @if(auth()->user()->hasPermission('delete_categories'))
                                    <th class="dt-no-sorting"><input type="checkbox" class="check_all"></th>
                                @endif
                                <th>{{trans('users.ID')}}</th>
                                <th>{{trans('users.Name')}}</th>
                                <th>{{trans('users.Related Products')}}</th>
                                <th class="dt-no-sorting">{{trans('users.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    @if(auth()->user()->hasPermission('delete_categories'))
                                    <td><input type="checkbox" name="items[]" class="items_checkbox" value="{{$category->id}}"></td>
                                    @endif
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                        <td><a href="{{route('getCategory',['category_id'=>$category->id])}}" class="btn btn-primary">{{trans('users.Related Products')}}</a></td>
                                    <td>
                                        @if(auth()->user()->hasPermission('update_categories'))
                                            <a href="{{route('categories.edit',$category->id)}}" class="btn btn-success">{{trans('users.Edit')}}</a>
                                        @else
                                            <a href="#" class="btn btn-success disabled">{{trans('users.Edit')}}</a>
                                        @endif
                                        @if(auth()->user()->hasPermission('delete_categories'))
                                            <a href="{{route('delete_categories',$category->id)}}" class="btn btn-danger delbtn">{{trans('users.Delete')}}</a>
                                        @else
                                            <a href="#" class="btn btn-danger disabled">{{trans('users.Delete')}}</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </form>

        </div>

    </div>

    @push('js')
        <script src="{{url('/')}}/admin/plugins/sweetalerts/sweetalert2.min.js"></script>
        <script src="{{url('/')}}/admin/plugins/sweetalerts/custom-sweetalert.js"></script>
        <!-- END THEME GLOBAL STYLE -->
        <script>
            $('.delbtn').on('click', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: 'btn btn-success btn-rounded',
                    cancelButtonClass: 'btn btn-danger btn-rounded mr-3',
                    buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                    title: '{{trans('users.Are you sure?')}}',
                    text: "{{trans('users.You wont be able to revert this!')}}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{trans('users.Yes, delete it!')}}',
                    cancelButtonText: '{{trans('users.No, cancel!')}}',
                    reverseButtons: true,
                    padding: '2em'
                }).then(function(result) {
                    if (result.value) {
                        window.location.href = link;
                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons(
                            '{{trans('users.Cancelled')}}',
                            '{{trans('users.Your imaginary file is safe :)')}}',
                            'error'
                        )
                    }
                })
            })
        </script>

        <script>
            $(document).on('click','.check_all',function(){
                $('input[class="items_checkbox"]:checkbox').each(function(){
                    if($('input[class="check_all"]:checkbox:checked').length == 0){
                        $(this).prop('checked',false);
                    }else{
                        $(this).prop('checked',true);
                    }
                })
            });
        </script>
        <script>
            $('.multidel').on('click', function (e) {
                e.preventDefault();
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: 'btn btn-success btn-rounded',
                    cancelButtonClass: 'btn btn-danger btn-rounded mr-3',
                    buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                    title: '{{trans('users.Are you sure?')}}',
                    text: "{{trans('users.You wont be able to revert this!')}}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{trans('users.Yes, delete it!')}}',
                    cancelButtonText: '{{trans('users.No, cancel!')}}',
                    reverseButtons: true,
                    padding: '2em'
                }).then(function(result) {
                    if (result.value) {
                        $('#mutlidel').submit();
                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons(
                            '{{trans('users.Cancelled')}}',
                            '{{trans('users.Your imaginary file is safe :)')}}',
                            'error'
                        )
                    }
                })
            })
        </script>
    @endpush
@endsection
