@component('mail::message')
# {{trans('users.Welcome').' ' . $data['data']->name}}

{{trans('users.Click The Link Below To Go To Reset Password Page')}}

@component('mail::button', ['url' => url('reset/password/'.$data['token'])])
{{trans('users.Click Here')}}
@endcomponent

{{trans('users.Thanks')}},<br>
{{ config('app.name') }}
@endcomponent
