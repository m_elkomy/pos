<div class="overlay"></div>
<div class="search-overlay"></div>

<!--  BEGIN SIDEBAR  -->
<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu">
                <a href="#dashboard" data-active="true" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        <span>{{trans('users.Dashboard')}}</span>
                    </div>
                </a>
            </li>

            @if(auth()->user()->hasPermission('read_users'))
                <li class="menu">
                    <a href="#users" data-toggle="collapse" {{active_menu('users')[0]}} class="dropdown-toggle {{active_menu('users')[1]}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                            <span>{{trans('users.Users')}}</span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                    <ul class="collapse submenu list-unstyled {{active_menu('users')[2]}}" id="users" data-parent="#accordionExample">
                        <li class="{{active_link('users')[0]}}">
                            <a href="{{aurl('users')}}"> {{trans('users.Users List')}} </a>
                        </li>
                        @if(auth()->user()->hasPermission('create_users'))
                            <li class="{{active_link('users/create')[0]}}">
                                <a href="{{route('users.create')}}"> {{trans('users.Add User')}} </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(auth()->user()->hasPermission('read_categories'))
                <li class="menu">
                    <a href="#categories" data-toggle="collapse" {{active_menu('categories')[0]}} class="dropdown-toggle {{active_menu('categories')[1]}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>                            <span>{{trans('users.Categories')}}</span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                    <ul class="collapse submenu list-unstyled {{active_menu('categories')[2]}}" id="categories" data-parent="#accordionExample">
                        <li class="{{active_link('categories')[0]}}">
                            <a href="{{aurl('categories')}}"> {{trans('users.Category List')}} </a>
                        </li>
                        @if(auth()->user()->hasPermission('create_categories'))
                            <li class="{{active_link('categories/create')[0]}}">
                                <a href="{{route('categories.create')}}"> {{trans('users.Add Category')}} </a>
                            </li>
                        @endif

                        @if(auth()->user()->hasPermission('create_categories'))
                            <li class="{{active_link('categories/multi_add')[0]}}">
                                <a href="{{aurl('categories/multi_add')}}"> {{trans('users.Multi Add Category')}} </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @if(auth()->user()->hasPermission('read_products'))
                <li class="menu">
                    <a href="#products" data-toggle="collapse" {{active_menu('products')[0]}} class="dropdown-toggle {{active_menu('products')[1]}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-monitor"><rect x="2" y="3" width="20" height="14" rx="2" ry="2"></rect><line x1="8" y1="21" x2="16" y2="21"></line><line x1="12" y1="17" x2="12" y2="21"></line></svg>                            <span>{{trans('users.Products')}}</span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                    <ul class="collapse submenu list-unstyled {{active_menu('products')[2]}}" id="products" data-parent="#accordionExample">
                        <li class="{{active_link('products')[0]}}">
                            <a href="{{aurl('products')}}"> {{trans('users.Products List')}} </a>
                        </li>
                        @if(auth()->user()->hasPermission('create_products'))
                            <li class="{{active_link('products/create')[0]}}">
                                <a href="{{route('products.create')}}"> {{trans('users.Add Product')}} </a>
                            </li>
                        @endif

                        @if(auth()->user()->hasPermission('create_products'))
                            <li class="{{active_link('products/multi_add')[0]}}">
                                <a href="{{aurl('products/multi_add')}}"> {{trans('users.Multi Add Product')}} </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(auth()->user()->hasPermission('read_orders'))
                <li class="menu">
                    <a href="#orders" data-toggle="collapse" {{active_menu('orders')[0]}} class="dropdown-toggle {{active_menu('orders')[1]}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-plus"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="12" y1="18" x2="12" y2="12"></line><line x1="9" y1="15" x2="15" y2="15"></line></svg>                            <span>{{trans('users.Orders')}}</span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                    <ul class="collapse submenu list-unstyled {{active_menu('orders')[2]}}" id="orders" data-parent="#accordionExample">
                        <li class="{{active_link('orders')[0]}}">
                            <a href="{{aurl('orders')}}"> {{trans('users.Orders List')}} </a>
                        </li>
                        @if(auth()->user()->hasPermission('create_orders'))
                            <li class="{{active_link('orders/create')[0]}}">
                                <a href="{{route('orders.create')}}"> {{trans('users.Add Order')}} </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

        </ul>

    </nav>

</div>
<!--  END SIDEBAR  -->
