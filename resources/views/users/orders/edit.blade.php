@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-accordions.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css" />

@endpush
@section('content')
    <div class="row layout-top-spacing">
        <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div id="accordionBasic" class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">
                    <div id="toggleAccordion">
                        @foreach($categories as $category)
                            <div class="card">
                                <div class="card-header" id="{{$category->name.'_'.$category->id}}">
                                    <section class="mb-0 mt-0">
                                        <div role="menu" class="collapsed" data-toggle="collapse" data-target="#{{str_replace(' ','_',$category->name)}}" aria-expanded="false" aria-controls="defaultAccordionThree">
                                            {{$category->name}} <div class="icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></div>
                                        </div>
                                    </section>
                                </div>
                                <div id="{{str_replace(' ','_',$category->name)}}" class="collapse" aria-labelledby="{{$category->name.'_'.$category->id}}" data-parent="#toggleAccordion">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped mb-4">
                                            <thead>
                                            <tr>
                                                <th>{{trans('users.ID')}}</th>
                                                <th>{{trans('users.Name')}}</th>
                                                <th>{{trans('users.Stock')}}</th>
                                                <th>{{trans('users.Sale Price')}}</th>
                                                <th class="text-center">{{trans('users.Action')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($category->products as $product)
                                                <tr>
                                                    <td>{{$product->id}}</td>
                                                    <td>{{$product->name}}</td>
                                                    <td>{{$product->stock}}</td>
                                                    <td>{{number_format($product->sale_price,2)}}</td>
                                                    <td>
                                                        <a href="" id="product-{{$product->id}}" data-name="{{$product->name}}" data-id="{{$product->id}}" data-price="{{$product->sale_price}}" class="btn {{in_array($product->id,$order->products->pluck('id')->toArray()) ? 'btn-default disabled':'btn-success'}} add-product-btn">+</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div id="accordionBasic" class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{trans('users.Orders')}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">
                    <div id="toggleAccordion">

                        <form method="post" action="{{route('orders.update',$order->id)}}">
                            {{csrf_field()}}
                            @method('put')
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped mb-4">
                                    <thead>
                                    <tr>
                                        <th>{{trans('users.Products')}}</th>
                                        <th>{{trans('users.Quantity')}}</th>
                                        <th>{{trans('users.Price')}}</th>
                                        <th>{{trans('users.Action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody class="order-list">
                                    @foreach($order->products as $product)
                                        <tr>
                                            <td>{{$product->name}}</td>
                                            <input type="hidden" name="products[]" value="{{$product->id}}">
                                            <td><input type="number" value="{{$product->pivot->quantity}}" name="quantities[]" min="1" data-price="{{number_format($product->sale_price,2)}}" class="form-control product-quantity"></td>
                                            <td class="product-price">
                                                {{number_format($product->sale_price * $product->pivot->quantity,2)}}
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-sm remove-product-btn" data-id="{{$product->id}}">-</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="mb-3">
                                    {{trans('users.Total Price')}} : <span class="total-price"> {{ number_format($order->total_price, 2) }}</span>
                                </div>
                                <button class="btn btn-info btn-block" id="add-order-form-btn">{{trans('users.Update')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{url('/')}}/admin/assets/js/scrollspyNav.js"></script>
        <script src="{{url('/')}}/admin/assets/js/components/ui-accordions.js"></script>
        <script src="{{url('/')}}/admin/assets/js/jquery.number.min.js"></script>
        <script>
            $(document).ready(function (){
                $('.add-product-btn').on('click',function(e){
                    e.preventDefault();
                    var name = $(this).data('name');
                    var id = $(this).data('id');
                    var price = $.number($(this).data('price'),2);

                    $(this).removeClass('btn-success').addClass('btn-default disabled');
                    var html = `<tr>
                                    <td>${name}</td>
                                    <input type="hidden" name="products[]" value="${id}">
                                    <td><input type="number" name="quantities[]" data-price="${price}" min="1" value="1" class="form-control product-quantity"></td>
                                    <td class="product-price">${price}</td>
                                    <td><button type="button" class="btn btn-danger remove-product-btn" data-id="${id}">-</button> </td>
                                </tr>`;
                    $('.order-list').append(html);
                    calculate_total();
                });
            });
            $('body').on('click','.remove-product-btn',function(e){
                e.preventDefault();
                var id = $(this).data('id');
                $(this).closest('tr').remove();
                $('#product-'+id).removeClass('btn-default disabled').addClass('btn-success');
                calculate_total();
            });

            function calculate_total(){
                var price = 0;
                $('.order-list .product-price').each(function(index){
                    price+=parseFloat($(this).html().replace(/,/g,''));
                })
                $('.total-price').html($.number(price,2));
                if(price>0){
                    $('#add-order-form-btn').removeClass('disabled');
                }else{
                    $('#add-order-form-btn').addClass('disabled');
                }
            }


            $('body').on('change keyup','.product-quantity',function(){
                var quantity = Number($(this).val());
                var price = parseFloat($(this).data('price').replace(/,/g,''));
                $(this).closest('tr').find('.product-price').html($.number(quantity * price,2));
                calculate_total();
            });
        </script>
    @endpush
@endsection
