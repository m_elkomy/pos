@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">


        <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div id="accordionBasic" class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">
                    <div id="toggleAccordion">
                         <div class="table-responsive orders-products-list">
                                <table class="table table-bordered table-striped mb-4">
                                    <thead>
                                    <tr>
                                        <th>{{trans('users.ID')}}</th>
                                        <th>{{trans('users.Name')}}</th>
                                        <th>{{trans('users.Quantity')}}</th>
                                        <th>{{trans('users.Price')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody class="order-list">
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{$product->id}}</td>
                                                <td>{{$product->name}}</td>
                                                <td>{{$product->pivot->quantity}}</td>
                                                <td>{{number_format($product->pivot->quantity * $product->sale_price,2)}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                             <div class="mb-3">
                                 {{trans('users.Total Price')}} : <span class="total-price"> {{ number_format($order->total_price, 2) }}</span>
                             </div>
                            </div>
                        <div>
                            <a href="" class="btn btn-block btn-dark print-btn">{{trans('users.Print')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @push('js')
        <script src="{{url('/')}}/admin/assets/js/printThis.js"></script>
        <script>
            $(document).on('click','.print-btn',function(e){
                e.preventDefault();
                    $('.orders-products-list').printThis();
            })
        </script>
    @endpush
@endsection
