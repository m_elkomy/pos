@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">


        <div class="col-lg-12 col-12  layout-spacing">
            @include('users.layout.message')

            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content">

                    <form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group mb-4">
                            <label for="name_ar">{{trans('users.Arabic Name')}}</label>
                            <input type="text" name="name_ar" class="form-control" id="name_ar" placeholder="{{trans('users.Arabic Name')}}" value="{{old('name_ar')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="name_en">{{trans('users.English Name')}}</label>
                            <input type="text" name="name_en" class="form-control" id="name_en" placeholder="{{trans('users.English Name')}}" value="{{old('name_en')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="category_id">{{trans('users.Category')}}</label>
                            <select class="form-control" name="category_id" id="category_id">
                                <option value="0">{{trans('users.Category')}}</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <label for="sale_price">{{trans('users.Sale Price')}}</label>
                            <input type="text" name="sale_price" class="form-control" id="sale_price" placeholder="{{trans('users.Sale Price')}}" value="{{old('sale_price')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="sale_price">{{trans('users.Purchase Price')}}</label>
                            <input type="text" name="purchase_price" class="form-control" id="purchase_price" placeholder="{{trans('users.Purchase Price')}}" value="{{old('purchase_price')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="stock">{{trans('users.Stock')}}</label>
                            <input type="number" min="1" name="stock" class="form-control" id="stock" placeholder="{{trans('users.Stock')}}" value="{{old('stock')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="image">{{trans('users.Image')}}</label>
                            <input type="file" class="form-control-file image" id="image" name="image">
                        </div>

                        <div class="form-group mb-4">
                            <label for="image">{{trans('users.Image Preview')}}</label>
                            <img src="{{asset('uploads/products/default.png')}}" width="100px" height="100px" class="img-thumbnail img-preview">
                        </div>

                        <button type="submit" class="btn btn-info">{{trans('users.Save')}}</button>
                        <a href="{{aurl('categories')}}" class="btn btn-danger">{{trans('users.Cancel')}}</a>
                    </form>

                </div>
            </div>
        </div>

    </div>
    @push('js')
        <script>
            $('.image').on('change',function(){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                } else {
                    $('.img-preview').attr('src', '');
                }
            });
        </script>
    @endpush
@endsection
