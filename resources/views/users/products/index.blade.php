@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/plugins/animate/animate.css" rel="stylesheet" type="text/css" />
    <script src="{{url('/')}}/admin/plugins/sweetalerts/promise-polyfill.js"></script>
    <link href="{{url('/')}}/admin/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">

        <div id="form_sizing" class="col-lg-12">
            <div class="seperator-header">
                <h4 class="">{{trans('users.Filtering')}}</h4>
            </div>
        </div>


        <div class="col-lg-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{trans('users.Category')}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">

                    <div class="row">

                        <div class="col-xl-12 mb-xl-0 mb-2">
                            <form method="get" action="{{route('getCategory')}}">
{{--                                {{csrf_field()}}--}}
                                <select class="form-control form-control-lg" id="category_id" name="category_id" onchange="this.form.submit()">
                                    <option value="0">{{trans('users.Category')}}</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" {{request()->category_id == $category->id ? 'selected':''}}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="row layout-top-spacing">

        @include('users.layout.message')
        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <form action="{{route('products_multi_delete')}}" id="mutlidel">
                {{csrf_field()}}
                @if(auth()->user()->hasPermission('delete_products'))
                <button type="submit" class="btn btn-danger mb-3 multidel">{{trans('users.Delete Selected')}}</button>
                @else
                    <button type="button" class="btn btn-danger mb-3 disabled">{{trans('users.Delete Selected')}}</button>
                @endif

                <div class="widget-content widget-content-area br-6">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                @if(auth()->user()->hasPermission('delete_products'))
                                <th class="dt-no-sorting"><input type="checkbox" class="check_all"></th>
                                @endif
                                <th>{{trans('users.ID')}}</th>
                                <th>{{trans('users.Image')}}</th>
                                <th>{{trans('users.Name')}}</th>
                                <th>{{trans('users.Stock')}}</th>
                                <th>{{trans('users.Purchase Price')}}</th>
                                <th>{{trans('users.Sale Price')}}</th>
                                <th>{{trans('users.Profit Percent')}}</th>
                                <th>{{trans('users.Category')}}</th>
                                <th class="dt-no-sorting">{{trans('users.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    @if(auth()->user()->hasPermission('delete_products'))
                                    <td><input type="checkbox" name="items[]" class="items_checkbox" value="{{$product->id}}"></td>
                                    @endif
                                    <td>{{$product->id}}</td>
                                    <td><img src="{{$product->image_path}}" width="50px" height="50px" class="img-thumbnail"></td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->stock}}</td>
                                    <td>{{$product->purchase_price}}</td>
                                    <td>{{$product->sale_price}}</td>
                                    <td>{{$product->profit_percent}} %</td>
                                    <td>{{$product->category->name}}</td>
                                    <td>
                                        @if(auth()->user()->hasPermission('update_products'))
                                            <a href="{{route('products.edit',$product->id)}}" class="btn btn-success">{{trans('users.Edit')}}</a>
                                        @else
                                            <a href="#" class="btn btn-success disabled">{{trans('users.Edit')}}</a>
                                        @endif
                                        @if(auth()->user()->hasPermission('delete_products'))
                                            <a href="{{route('delete_products',$product->id)}}" class="btn btn-danger delbtn">{{trans('users.Delete')}}</a>
                                        @else
                                            <a href="#" class="btn btn-danger disabled">{{trans('users.Delete')}}</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </form>

        </div>

    </div>

    @push('js')
        <script src="{{url('/')}}/admin/plugins/sweetalerts/sweetalert2.min.js"></script>
        <script src="{{url('/')}}/admin/plugins/sweetalerts/custom-sweetalert.js"></script>
        <!-- END THEME GLOBAL STYLE -->
        <script>
            $('.delbtn').on('click', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: 'btn btn-success btn-rounded',
                    cancelButtonClass: 'btn btn-danger btn-rounded mr-3',
                    buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                    title: '{{trans('users.Are you sure?')}}',
                    text: "{{trans('users.You wont be able to revert this!')}}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{trans('users.Yes, delete it!')}}',
                    cancelButtonText: '{{trans('users.No, cancel!')}}',
                    reverseButtons: true,
                    padding: '2em'
                }).then(function(result) {
                    if (result.value) {
                        window.location.href = link;
                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons(
                            '{{trans('users.Cancelled')}}',
                            '{{trans('users.Your imaginary file is safe :)')}}',
                            'error'
                        )
                    }
                })
            })
        </script>

        <script>
            $(document).on('click','.check_all',function(){
                $('input[class="items_checkbox"]:checkbox').each(function(){
                    if($('input[class="check_all"]:checkbox:checked').length == 0){
                        $(this).prop('checked',false);
                    }else{
                        $(this).prop('checked',true);
                    }
                })
            });
        </script>
        <script>
            $('.multidel').on('click', function (e) {
                e.preventDefault();
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: 'btn btn-success btn-rounded',
                    cancelButtonClass: 'btn btn-danger btn-rounded mr-3',
                    buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                    title: '{{trans('users.Are you sure?')}}',
                    text: "{{trans('users.You wont be able to revert this!')}}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{trans('users.Yes, delete it!')}}',
                    cancelButtonText: '{{trans('users.No, cancel!')}}',
                    reverseButtons: true,
                    padding: '2em'
                }).then(function(result) {
                    if (result.value) {
                        $('#mutlidel').submit();
                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons(
                            '{{trans('users.Cancelled')}}',
                            '{{trans('users.Your imaginary file is safe :)')}}',
                            'error'
                        )
                    }
                })
            })
        </script>
    @endpush
@endsection
