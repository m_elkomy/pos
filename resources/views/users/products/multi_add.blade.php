@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">


        <div class="col-lg-12 col-12  layout-spacing">
            @include('users.layout.message')

            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content">

                    <form method="post" action="{{route('products_multi_add_store')}}" class="repeater">
                        {{csrf_field()}}
                            <div data-repeater-list="products_list">
                                <div data-repeater-item>
                                    <div class="row mb-4">
                                        <div class="col">
                                            <select class="form-control" name="category_id" id="category_id">
                                                <option value="0">{{trans('users.Category')}}</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <input type="text" name="name_ar" class="form-control" placeholder="{{trans('users.Arabic Name')}}">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="{{trans('users.English Name')}}" name="name_en">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="{{trans('users.Purchase Price')}}" name="purchase_price">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="{{trans('users.Sale Price')}}" name="sale_price">
                                        </div>
                                        <div class="col">
                                            <input type="number" min="0" class="form-control" placeholder="{{trans('users.Stock')}}" name="stock">
                                        </div>
                                        <div class="col">
                                            <button type="button" data-repeater-delete class="btn btn-danger">-</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        <button type="button" data-repeater-create class="btn btn-success">{{trans('users.Add')}}</button>
                        <button type="submit" class="btn btn-info">{{trans('users.Save')}}</button>
                        <a href="{{aurl('products')}}" class="btn btn-danger">{{trans('users.Cancel')}}</a>
                    </form>

                </div>
            </div>
        </div>

    </div>
    @push('js')
    <script src="{{url('/')}}/admin/assets/js/repeater.js"></script>
    @endpush

@endsection
