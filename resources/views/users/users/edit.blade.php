@extends('users.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">


        <div class="col-lg-12 col-12  layout-spacing">
            @include('users.layout.message')

            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>

                <div class="widget-content">

                    <form method="post" action="{{route('users.update',$user->id)}}" enctype="multipart/form-data">
                        @method('put')
                        {{csrf_field()}}
                        <div class="form-group mb-4">
                            <label for="name">{{trans('users.Name')}}</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="{{trans('users.Name')}}" value="{{$user->name}}">
                        </div>
                        <div class="form-group mb-4">
                            <label for="email">{{trans('users.Email')}}</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="{{trans('users.Email')}}" value="{{$user->email}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="password">{{trans('users.Password')}}</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="{{trans('users.Password')}}">
                        </div>

                        <div class="form-group mb-4">
                            <label for="password_confirmation">{{trans('users.Password Confirmation')}}</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="{{trans('users.Password Confirmation')}}">
                        </div>
                        <div class="form-group mb-4">
                            <label for="image">{{trans('users.Image')}}</label>
                            <input type="file" class="form-control-file image" id="image" name="image">
                        </div>

                        <div class="form-group mb-4">
                            <label for="image">{{trans('users.Image Preview')}}</label>
                            <img src="{{$user->image_path}}" width="100px" height="100px" class="img-thumbnail img-preview">
                        </div>


                        <div class="statbox widget box box-shadow mb-3">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>{{trans('users.Permissions')}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-content animated-underline-content">

                                @php
                                    $models = ['users','categories','products'];
                                    $maps = ['create','read','update','delete'];
                                @endphp
                                <ul class="nav nav-tabs  mb-3" id="animateLine" role="tablist">
                                    @foreach($models as $index=>$model)
                                        <li class="nav-item">
                                            <a class="nav-link {{$index==0?'active':''}}" id="animated-underline-{{$model}}-tab" data-toggle="tab" href="#animated-underline-{{$model}}" role="tab" aria-controls="animated-underline-home" aria-selected="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                                {{trans('users.'.$model)}}</a>
                                        </li>
                                    @endforeach
                                </ul>

                                <div class="tab-content" id="animateLineContent-4">
                                    @foreach($models as $index=>$model)
                                        <div class="tab-pane fade show {{$index==0?'active':''}}" id="animated-underline-{{$model}}" role="tabpanel" aria-labelledby="animated-underline-{{$model}}-tab">
                                            @foreach($maps as $index=>$map)
                                                <label><input type="checkbox" name="permissions[]" {{$user->hasPermission($map.'_'.$model) ? 'checked':''}} value="{{$map .'_'.$model}}"> {{trans('users.'.$map)}}</label>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                        <a href="{{aurl('users')}}" class="btn btn-danger">{{trans('users.Cancel')}}</a>
                        <button type="submit" class="btn btn-info">{{trans('users.update')}}</button>
                    </form>

                </div>
            </div>
        </div>

    </div>
    @push('js')
        <script>
            $('.image').on('change',function(){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                } else {
                    $('.img-preview').attr('src', '');
                }
            });
        </script>
    @endpush
@endsection
