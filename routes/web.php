<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'serialNumber'],function(){
    //add language prefix
    Route::group(
        [
            'prefix' => LaravelLocalization::setLocale(),
            'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
        ], function(){


        //add middleware guest to all route
        Route::group(['middleware'=>'guest'],function(){
            //add namespace user to all route
            Route::group(['namespace'=>'User'],function(){
                //route to function that return login view
                Route::get('login','UserAuthController@login')->name('login');
                //route to function that handle users login
                Route::post('login','UserAuthController@do_login')->name('do_login');
                //route to function that return forget password view
                Route::get('forget/password','UserAuthController@forget_password')->name('forget_password');
                //route to function that handle create token for user
                Route::post('forget/password','UserAuthController@forget_password_post')->name('forget_password_post');
                //route to function that return reset password view
                Route::get('reset/password/{token}','UserAuthController@reset_password')->name('reset_password');
                //route to function that reset user password
                Route::post('reset/password/{token}','UserAuthController@reset_password_final')->name('reset_password_final');
            });
        });


        //assign middleware to all route
        Route::group(['middleware'=>'auth:web'],function(){
            //redirect route to dashboard
            Route::redirect('/','dashboard');

            //prefix all route with dashboard
            Route::group(['prefix'=>'dashboard'],function(){
                //route to return home view
                Route::get('/',function(){
                    return view('users.index',['title'=>trans('users.Dashboard')]);
                });

                //route to users resource
                Route::namespace('User')->group(function(){
                    Route::get('users/delete/{id}','UserController@destroy')->name('delete_users');
                    Route::resource('users','UserController');
                    //rotue to logout function that handle user logout

                    Route::any('logout','UserAuthController@logout')->name('logout');

                });
                //route to resource category
                Route::namespace('Category')->group(function(){
                    Route::get('categories/multi_add','CategoryController@multi_add')->name('multi_add');
                    Route::post('categories/multi_add','CategoryController@multi_store')->name('categories_multi_add_store');
                    Route::get('categories/multi_delete','CategoryController@multi_delete')->name('category_multi_delete');
                    Route::get('categories/delete/{id}','CategoryController@destroy')->name('delete_categories');
                    Route::resource('categories','CategoryController');
                });


                //route to resource products
                Route::namespace('Product')->group(function(){
                    Route::get('products/category','ProductController@getcategory')->name('getCategory');
                    Route::get('products/multi_add','ProductController@multi_add')->name('multi_add');
                    Route::post('products/multi_add','ProductController@multi_store')->name('products_multi_add_store');
                    Route::get('products/multi_delete','ProductController@multi_delete')->name('products_multi_delete');
                    Route::get('products/delete/{id}','ProductController@destroy')->name('delete_products');
                    Route::resource('products','ProductController');
                });


                //route to resource products
                Route::namespace('Order')->group(function(){
                    Route::get('orders/multi_add','OrderController@multi_add')->name('multi_add');
                    Route::post('orders/multi_add','OrderController@multi_store')->name('orders_multi_add_store');
                    Route::get('orders/delete/{id}','OrderController@destroy')->name('delete_orders');
                    Route::resource('orders','OrderController');
                });

            });
        });




    });
});
Route::get('notfound',function(){
    return view('users.notFound',['title'=>trans('users.Not Found')]);
})->name('notFound');

